package com.dimon95me.testtask.ui.main.recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ListView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.dimon95me.test.model.InStock
import com.dimon95me.testtask.R
import com.dimon95me.testtask.databinding.InStockItemBinding

class InStocksAdapter(): RecyclerView.Adapter<InStocksAdapter.ViewHolder>() {

    private var inStocks = ArrayList<InStock>()

    fun updateStocks(inStocks: ArrayList<InStock>) {
        this.inStocks = inStocks
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<InStockItemBinding>(inflater, R.layout.in_stock_item, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(inStocks.get(position))
    }

    override fun getItemCount(): Int {
        return inStocks.size
    }

    class ViewHolder(private val binding: InStockItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(inStock: InStock) {
            binding.inStock = inStock
        }

    }
}
