package com.dimon95me.testtask.ui.main

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dimon95me.test.model.ProductsList
import com.dimon95me.testtask.api.network.ProductApi
import com.dimon95me.testtask.api.network.RetrofitInstance
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket

class MainViewModel : ViewModel() {
    var productsLiveData = MutableLiveData<ProductsList>()
        private set
    var checkInternetConnectionLiveData = MutableLiveData<Boolean>()
        private set
    private var productsApi = RetrofitInstance.getRetrofitInstance().create(ProductApi::class.java)

    fun getProductsFromServer() {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val response = productsApi.getProductsList()
                if (response.body() != null) {
                    productsLiveData.postValue(response.body())
                }
            } catch (e: IOException) {
                Log.e("ERROR", "getRetrofitInstance: ", e)
                checkInternetConnection()
            }
        }
    }

    fun checkInternetConnection() {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                var socket = Socket()
                socket.connect(InetSocketAddress("8.8.8.8", 53), 1500)
                socket.close()
                checkInternetConnectionLiveData.postValue(true)
            } catch (e: IOException) {
                checkInternetConnectionLiveData.postValue(false)
            }

        }
    }
}