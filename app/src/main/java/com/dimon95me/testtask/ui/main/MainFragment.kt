package com.dimon95me.testtask.ui.main

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.dimon95me.test.ui.main.recyclerProducts.ProductsAdapter
import com.dimon95me.testtask.R
import com.dimon95me.testtask.databinding.MainFragmentBinding

class MainFragment : Fragment(R.layout.main_fragment) {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel
    private lateinit var binding: MainFragmentBinding
    private lateinit var adapterProducts: ProductsAdapter


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        binding = MainFragmentBinding.bind(view)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        initRecycler()

        viewModel.productsLiveData.observe(viewLifecycleOwner, Observer {
            adapterProducts.updateItems(it.products)
        })

        viewModel.getProductsFromServer()
    }

    private fun initRecycler() {
        binding.mainRecycler.apply {
            adapterProducts = ProductsAdapter()
            this.adapter = adapterProducts
            this.layoutManager = LinearLayoutManager(context)
        }
    }

    override fun onDetach() {
        super.onDetach()
        viewModel.productsLiveData.removeObservers(this)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)

        val search = menu.findItem(R.id.search)
        val searchView = search.actionView as SearchView
        searchView.queryHint = "Enter your request"

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                adapterProducts.filterByText(newText)
                return true
            }

        })
    }

}