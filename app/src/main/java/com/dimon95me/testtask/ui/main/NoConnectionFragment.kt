package com.dimon95me.testtask.ui.main

import androidx.fragment.app.Fragment
import com.dimon95me.testtask.R

class NoConnectionFragment : Fragment(R.layout.fragment_no_connection) {

    companion object {
        fun newInstance() = NoConnectionFragment()
    }

}