package com.dimon95me.test.ui.main.recyclerProducts

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.dimon95me.test.model.InStock
import com.dimon95me.test.model.Product
import com.dimon95me.testtask.R
import com.dimon95me.testtask.databinding.ProductItemBinding
import com.dimon95me.testtask.ui.main.recycler.InStocksAdapter
import com.google.android.flexbox.FlexboxLayoutManager

class ProductsAdapter() : RecyclerView.Adapter<ProductsAdapter.ViewHolder>() {

    private var items = ArrayList<Product>()
    private var filterItems = ArrayList<Product>()

    fun updateItems(list: ArrayList<Product>) {
        items = list
        filterItems.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ProductItemBinding>(
            inflater,
            R.layout.product_item,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items.get(position))
    }

    fun sortByName() {
        items.sortBy { it.name }
        notifyDataSetChanged()
    }

    fun sortByPrice() {
        items.sortBy { it.price }
        notifyDataSetChanged()
    }

    fun filterByText(newText: String?) {
        items = if (newText != null && newText.isEmpty()) {
            filterItems
        } else {
            filterItems.filter {
                it.name.contains(
                    newText.toString(),
                    true
                )
            } as ArrayList<Product>
        }
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ProductItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private var inStockAdapter: InStocksAdapter = InStocksAdapter()

        fun bind(product: Product) {
            binding.product = product

            var newList = ArrayList<InStock>()
            newList.addAll(product.in_stocks)
            newList.add(product.in_other.getInStockFormat())
            newList.add(product.in_waiting.getInStockFormat())
            initInStockRecycler(newList)
        }

        private fun initInStockRecycler(inStocks: List<InStock>) {
            inStockAdapter.updateStocks(inStocks as ArrayList<InStock>)
            binding.flexRecycler.apply {
                adapter = inStockAdapter
                layoutManager = FlexboxLayoutManager(binding.root.context)
            }
        }
    }

}
