package com.dimon95me.testtask

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.dimon95me.testtask.ui.main.MainFragment
import com.dimon95me.testtask.ui.main.MainViewModel
import com.dimon95me.testtask.ui.main.NoConnectionFragment

class MainActivity : AppCompatActivity() {

    lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        supportFragmentManager.beginTransaction()
            .replace(R.id.container, MainFragment.newInstance())
            .commitNow()

        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        viewModel.checkInternetConnectionLiveData.observe(this, Observer {
            if (it) {
                if (savedInstanceState == null) {
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.container, MainFragment.newInstance())
                        .commitNow()
                }
            } else {
                if (savedInstanceState == null) {
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.container, NoConnectionFragment.newInstance())
                        .commitNow()
                }
            }
        })
        viewModel.checkInternetConnection()
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.checkInternetConnectionLiveData.removeObservers(this)
    }

}