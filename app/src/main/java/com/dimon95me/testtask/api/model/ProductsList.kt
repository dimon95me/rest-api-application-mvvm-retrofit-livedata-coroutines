package com.dimon95me.test.model

import com.google.gson.annotations.SerializedName

data class ProductsList(
    @SerializedName("products") val products: ArrayList<Product>
)