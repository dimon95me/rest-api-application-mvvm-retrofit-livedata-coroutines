package com.dimon95me.test.model

import com.google.gson.annotations.SerializedName

data class InOther(

    @SerializedName("uuid") val uuid: String,
    @SerializedName("name") val name: String,
    @SerializedName("quantity") val quantity: String


) {
    fun getInStockFormat(): InStock {
        return InStock(name, uuid, quantity, ArrayList<ContractQuantity>())
    }
}
