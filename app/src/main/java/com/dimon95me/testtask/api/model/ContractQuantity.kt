package com.dimon95me.test.model

import com.google.gson.annotations.SerializedName

data class ContractQuantity(

    @SerializedName("f") val f: Int,
    @SerializedName("quantity") val quantity: String
)