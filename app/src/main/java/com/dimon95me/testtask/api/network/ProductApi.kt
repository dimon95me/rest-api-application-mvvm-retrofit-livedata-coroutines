package com.dimon95me.testtask.api.network

import com.dimon95me.test.model.ProductsList
import retrofit2.Response
import retrofit2.http.GET

interface ProductApi {

    @GET("test_data_products.json")
    suspend fun getProductsList() :Response<ProductsList>

}