package com.dimon95me.test.model

import com.google.gson.annotations.SerializedName

data class Product(

	@SerializedName("uuid") val uuid: String,
	@SerializedName("special_offer") val special_offer: Boolean,
	@SerializedName("name") val name: String,
	@SerializedName("default_image") val default_image: String,
	@SerializedName("new_product") val new_product: Boolean,
	@SerializedName("tools") val tools: Boolean,
	@SerializedName("brand") val brand: String,
	@SerializedName("promos") val promos: List<String>,
	@SerializedName("article") val article: String,
	@SerializedName("promotion") val promotion: Boolean,
	@SerializedName("highlight") val highlight: String,
	@SerializedName("price") val price: Double,
	@SerializedName("in_stocks") val in_stocks: List<InStock>,
	@SerializedName("in_others") val in_other: InOther,
	@SerializedName("in_waiting") val in_waiting: InWaiting,
	@SerializedName("available") val available: Boolean,
	@SerializedName("currency_name") val currency_name: String,
	@SerializedName("found_by") val found_by: String
)