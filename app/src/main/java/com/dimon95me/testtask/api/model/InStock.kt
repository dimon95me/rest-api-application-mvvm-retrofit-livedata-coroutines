package com.dimon95me.test.model

import com.google.gson.annotations.SerializedName


data class InStock(

    @SerializedName("name") val name: String,
    @SerializedName("uuid") val uuid: String,
    @SerializedName("quantity") val quantity: String,
    @SerializedName("contract_quantities") val contract_quantities: List<ContractQuantity>
)